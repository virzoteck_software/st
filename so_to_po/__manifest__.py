# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Create Purchase Order From Sale Order',
    'version': '2.0',
    'category': 'Sales',
    'complexity': 'easy',
    'author':'Virzoteck softwares and solutions',
    'website': '',
    'description': """This module convert PO From SO.This module Works on both community and enterprise version 11""",
    'depends': ['purchase','sale','base','payment','sale_management'],
    'data': [
        'views/sale_view.xml',
        'views/purchase_view.xml',
        'views/sale_config_view.xml'
        
    ],
    'installable': True,
    'auto_install': True,
    # 'images': ['static/description/background.png',],
    'license': 'LGPL-3',
    "price":28,
    "currency": "EUR"
}