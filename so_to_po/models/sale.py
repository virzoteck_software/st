# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, models, fields, _
import time
from odoo.tests.common import TransactionCase
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import base64


class SaleOrder(models.Model):
    _inherit = 'sale.order'
    
    def set_values(self):
        super(SaleOrder, self).set_values()
        defaults = self.env['res.config.settings'].default_get([])
        print ('defaults======defaults==%s' % defaults)
        if defaults.get('default_allow_draft',False):
            allow_draft=defaults['default_allow_draft']
            if allow_draft:
                self.po_state=True
            else:
                return False
    



      
    
    @api.multi
    def _get_default_state(self):
        defaults = self.env['res.config.settings'].default_get([])
        print ('partner_id======defaults==%s' % self.id)
        if defaults.get('default_allow_draft',False):
            allow_draft=defaults['default_allow_draft']
            if allow_draft:
                return True
            else:
                return False
            

    @api.model
    def get_default_po_state(self, fields):
        rec = super(SaleOrder, self).default_get(fields)  
        print ('partner_id=yyyyyyyyyyyy=====defaults==%s' % self.id)
        s
        return rec
    

    @api.model
    def create(self, values):
        print ('partner_id=yyyyyyyyyyyy===ssss==defaults==%s' %values)

        rec = super(SaleOrder, self).create(values)
        return rec


        
    
    po_generated = fields.Boolean("Po Generated")
    po_state = fields.Boolean("check boolean for po",default="")


    def action_convert_to_po(self):
        purchase_order_obj=self.env['purchase.order']
        purchase_order_line_obj=self.env['purchase.order.line']
        
        defaults = self.env['res.config.settings'].default_get([])
        po_state=defaults['default_po_state']


        partner_id=self.partner_id.id

        print ('partner_id========%s' % po_state)
        

        po=purchase_order_obj.create({'partner_id':partner_id,'sale_order_id':self.id})
        self.write({'po_generated':True})

        po_dic={}

        for so_data in self.order_line:
            po_dic['name']=so_data.product_id.name
            po_dic['product_id']=so_data.product_id.id
            po_dic['product_qty']=so_data.product_uom_qty
            po_dic['price_unit']=so_data.price_unit
            po_dic['product_uom']=so_data.product_uom.id
            po_dic['date_planned']=datetime.today().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
            po_dic['order_id']=po.id
            
            purchase_order_line_obj.create(po_dic)
            po_dic={}
            
        if po_state == 'purchase':
            po.button_confirm()
            
        if po_state == 'sent':
            mail_list = []
            mail_list.append(self.partner_id.email)
            self.send_mail(po.id)  
            po.write({'state':'sent'})
#            write state
#            self.send_invoice_mail(po.id, mail_list, 'RFQ')
            
            
        return  True
    
    @api.multi
    def show_related_purchase_order(self):
        purchase_order_obj=self.env['purchase.order']
        view_id = self.env.ref('purchase.purchase_order_form').id
        context = self._context.copy()
        po_id=purchase_order_obj.search([('sale_order_id','=',self.id)])
        
        return {
            'name':'form_name',
            'view_type':'form',
            'view_mode':'tree',
            'views' : [(view_id,'form')],
            'res_model':'purchase.order',
            'view_id':view_id,
            'type':'ir.actions.act_window',
            'res_id':po_id[0].id,
#            'target':'new',
            'context':context,
        }
        
    @api.multi    
    def gen_rpt(self,purchase_id):
        purchase_order_obj = self.env['purchase.order']
        attachment_obj = self.env['ir.attachment']
        
#        pdf = self.env['report'].sudo().get_pdf([purchase_id], 'purchase.report_purchaseorder')

#        report_name = self.ev['mail.template'].render_template(template.report_name, template.model, res_id)
#        report = template.report_template
        from odoo.http import request
        result, format = request.env.ref('purchase.action_report_purchase_order').sudo().render_qweb_pdf([purchase_id])
#        print ('partner_id========%s' % result)
#        print ('partner_id====0000====%s' % result)


        # TODO in trunk, change return format to binary to match message_post expected format
        result = base64.b64encode(result)
#        if not report_name:
#            report_name = 'report.' + report_service
#        ext = "." + formatatt
#        if not report_name.endswith(ext):
#            report_name += ext
#        attachments.append((report_name, result))
#        results[res_id]['attachments'] = attachments


        return self.env['ir.attachment'].create({
        'name': 'Cats',
        'type': 'binary',
        'datas': result,
        'datas_fname' :'k.pdf',
        'res_model': 'account.invoice',
        'res_id': purchase_id,
         'mimetype' : 'application/pdf'})

#        for record in invoice_obj.browse(cr, SUPERUSER_ID, ids, context=context):
#            ir_actions_report = self.pool.get('ir.actions.report.xml')
#
#            matching_reports = ir_actions_report.search(cr, SUPERUSER_ID, [('model', '=', 'account.invoice'),
#                                                                           ('report_type', '=', 'qweb-pdf')])
#
#            if matching_reports:
#                report = ir_actions_report.browse(cr, SUPERUSER_ID, matching_reports[0])
#                report_service = 'report.' + report.report_name
#
#                result, format = openerp.report.render_report(cr, SUPERUSER_ID, [record.id], report.report_name,
#                                                              {'model': 'account.invoice'}, context=context)
#                #                #print '----------',result
#                # (result, format) = service.create(cr, uid, [record.id], {'model': 'hr.analytic.timesheet'}, context=context)
#                eval_context = {'time': time, 'object': record}
#                #                if not report.attachment or not eval(report.attachment, eval_context):
#
#                # no auto-saving of report as attachment, need to do it manually
#                result = base64.b64encode(result)
#                file_name = re.sub(r'[^a-zA-Z0-9_-]', '_', report.report_name)
#                file_name += ".pdf"
#                attach_id = attachment_obj.search(cr, SUPERUSER_ID, [('name', '=', file_name),
#                                                                     ('res_model', '=', 'hr.analytic.timesheet'),
#                                                                     ('res_id', '=', record.id)])
#                if len(attach_id):
#                    #                    _logger.error('Deleted List %s', attach_id)
#                    attachment_obj.unlink(cr, uid, attach_id, context)
#                    attachment_id = attach_id[0]
#
#                attachment_id = attachment_obj.create(cr, SUPERUSER_ID,
#                                                      {
#                                                          'name': file_name,
#                                                          'datas': result,
#                                                          'datas_fname': file_name,
#                                                          'res_model': 'account.invoice',
#                                                          'res_id': record.id,
#                                                          'type': 'binary'
#                                                      }, context=context)
                #                cr.commit()
                #                _logger.error('In project task listttttttttttttttttttttttttttttt  %s', attachment_id)
#                return attachment_id        
        
    @api.multi
    def send_invoice_mail(self,get_purchase_id, mail_list, send_msg):
        ir_mail_server = self.env['ir.mail_server']
        # fetch all server
        all_mail_server_id = ir_mail_server.search([])
        # fetching bydefaultly one mail server
        if len(all_mail_server_id):
            server_id = all_mail_server_id[0]
        attachment_id = self.gen_rpt(get_purchase_id)
        if attachment_id:
            attachments = []
            ir_attachment = self.env['ir.attachment']

            for attachment in attachment_id:
                print ('attachment========%s' % attachment.name)
                attachments = [(attachment.name, attachment.datas)]
                
            attachments = [(a['datas_fname'], base64.b64decode(a['datas']), a['mimetype'])
                           for a in attachment_id.sudo().read(['datas_fname', 'datas', 'mimetype'])]



            msg = ir_mail_server.build_email(
                email_from=server_id.smtp_user,
                email_to=mail_list,
                subject=send_msg,
                body=send_msg,
                body_alternative=send_msg,
                reply_to=mail_list,
                attachments=attachments,
                object_id=[],
                subtype='html'
            )
            # print'&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'
            res = ir_mail_server.send_email(msg, mail_server_id=server_id.id)

        return True
    
    @api.multi    
    def send_mail(self,purchase_id):
        email_template_obj = self.env['mail.template']
#        template_ids = email_template_obj.search([('name','=','RFQ - Send by Email'),('model_id.model', '=', 'purchase.order')])
        template_ids = self.env.ref('purchase.email_template_edi_purchase')
        values={}
        if template_ids:
              email=self.env['mail.template'].browse(template_ids.id).send_mail(purchase_id,force_send=True)
              print ('attachment========%s' % email)
#              self.env['mail.mail'].browse(email).send()
        return True
          

    

        
 