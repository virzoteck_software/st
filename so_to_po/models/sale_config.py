from odoo import api, models, fields, _

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    default_po_state = fields.Selection([
        ('draft','RFQ'),
        ('sent', 'RFQ Sent'),
        ('purchase','Purchase order')],
        default='draft',default_model='sale.config.cus')
        
    default_allow_draft= fields.Boolean("Allow onvert on draft state",default_model='sale.config.cus')
    default_allow_so= fields.Boolean("Allow onvert on Sale order state",default_model='sale.config.cus')
        
        
  
class sale_config_cus(models.Model):

    _name = 'sale.config.cus'

    name=fields.Char('Name')
    po_state = fields.Selection([
        ('draft','Quotation'),
        ('sent', 'Quotation Sent'),
        ('purchase','Purchase order')],
       'Default PO State')
         
    allow_draft= fields.Boolean("Allow onvert on draft state")
    allow_so= fields.Boolean("Allow onvert on Sale order state")

   
    


