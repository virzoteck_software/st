# -*- coding: utf-8 -*-
from datetime import datetime
from dateutil.relativedelta import relativedelta

from odoo import api, models, fields, _
import time
from odoo.tests.common import TransactionCase
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    
    sale_order_id = fields.Many2one('sale.order', string='Sale Order')
    
    @api.model
    def create(self, vals):
        purchase_order=super(PurchaseOrder, self).create(vals)    
        sale_order_obj=self.env['sale.order']
        print ('vals========%s' % vals)
        if vals.get('sale_order_id',False):
              order_name= purchase_order.name+'-'+sale_order_obj.browse(vals.get('sale_order_id')).name
              purchase_order.write({'name':order_name})
            
        return purchase_order
    
    
    
